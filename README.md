# PrivatePod1

[![CI Status](https://img.shields.io/travis/imemessenger@gmail.com/PrivatePod1.svg?style=flat)](https://travis-ci.org/imemessenger@gmail.com/PrivatePod1)
[![Version](https://img.shields.io/cocoapods/v/PrivatePod1.svg?style=flat)](https://cocoapods.org/pods/PrivatePod1)
[![License](https://img.shields.io/cocoapods/l/PrivatePod1.svg?style=flat)](https://cocoapods.org/pods/PrivatePod1)
[![Platform](https://img.shields.io/cocoapods/p/PrivatePod1.svg?style=flat)](https://cocoapods.org/pods/PrivatePod1)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PrivatePod1 is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PrivatePod1'
```

## Author

Alexander Shinkarenko

## License

PrivatePod1 is available under the MIT license. See the LICENSE file for more info.
