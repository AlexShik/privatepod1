# PrivatePod2

[![CI Status](https://img.shields.io/travis/imemessenger@gmail.com/PrivatePod2.svg?style=flat)](https://travis-ci.org/imemessenger@gmail.com/PrivatePod2)
[![Version](https://img.shields.io/cocoapods/v/PrivatePod2.svg?style=flat)](https://cocoapods.org/pods/PrivatePod2)
[![License](https://img.shields.io/cocoapods/l/PrivatePod2.svg?style=flat)](https://cocoapods.org/pods/PrivatePod2)
[![Platform](https://img.shields.io/cocoapods/p/PrivatePod2.svg?style=flat)](https://cocoapods.org/pods/PrivatePod2)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PrivatePod2 is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PrivatePod2'
```

## Author

Alexander Shinkarenko

## License

PrivatePod2 is available under the MIT license. See the LICENSE file for more info.
